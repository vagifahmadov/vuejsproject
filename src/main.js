import Vue from 'vue';
import App from './App.vue';
import VueRouter from 'vue-router';
import VuesiteHome from './pages/home.vue';
import Vuesite404 from './pages/404.vue';
import VuesiteUserprofile from './pages/userProfile.vue';
import VuesiteAboutus from './pages/aboutUs.vue';
import VuesiteTopics from './pages/topics.vue';
import VuesiteContact from './pages/contact.vue';
import VuesiteLogin from './admin/pages/login.vue';
import VuesiteIndex from './admin/pages/index.vue';

Vue.use(VueRouter);
const router = new VueRouter({
  routes: [
    // site side
    {
      path: '/',
      component: VuesiteHome,
      meta: {title: 'Home pag',  auth: true}
    },
    {
      path: '/user',
      component: VuesiteUserprofile,
      meta: {title: 'User', auth: true}
    },
    {
      path: '/aboutus',
      component: VuesiteAboutus,
      meta: {title: 'Kompanya haqqında',  auth: true}
    },
    {
      path: '/topics',
      component: VuesiteTopics,
      meta: {title: 'Fənlər',  auth: true}
    },
    {
      path: '/contact',
      component: VuesiteContact,
      meta: {title: 'Əlaqə',  auth: true}
    },    
    // admin side
    {
      path: '/admin',
      component: VuesiteIndex,
      meta: {title: 'İdarə etmə paneli', auth: true}
    },
    {
      path: '/admin/login',
      component: VuesiteLogin,
      meta: {title: 'İstifadəçi girişi', auth: true}
    },
    // not found
    {
      path: '/*',
      component: Vuesite404,
      meta: {title: 'Dostum, səhifə tapılmadı',  auth: true}
    }
  ],
  mode: 'history'
});

router.beforeEach((to, from, next) => {
  document.title = `${to.meta.title}`;
  next();
})

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})

